# This is the source for machine learning
# 这个代码是为了 机器学习识别 的后台代码

import torch
import torch.nn as nn
import torch.nn.functional as F

import torchvision
import torchvision.models as models
import torchvision.transforms as transforms

from PIL import Image

import os
import numpy as np

from keras.preprocessing import image

from matplotlib import pyplot as plt

def main():
    resnet50 = models.resnet50(pretrained=True)
    resnet50.eval()

    # Read the image from localhost
    img_path = '/Users/mickey/PycharmProjects/TestSocket/image/cat.jpg'
    # img = PIL.load(img_path, target_size=(224, 224))
    img = Image.open(img_path).convert('RGB')
    plt.imshow(img)

    # input_image = transforms.ToTensor()(img)

    # input_image = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])(input_image)
    # input_image = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])(input_image)
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    t = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        normalize,
    ])

    input_image = t(img)
    input_image = input_image.unsqueeze(dim=0)
    outputs = resnet50(input_image)

    outputs = torch.stack([nn.Softmax(dim=0)(i) for i in outputs])
    # outputs = outputs.mean(0)

    print(outputs)

    print(outputs.max(dim=1))

    # top_k = 10
    # outputs_numpy = outputs.detach().numpy()
    #
    # top_k_idx = outputs_numpy.argsort()[::-1][::top_k]
    #
    # print(top_k_idx)
    # print(outputs_numpy[top_k_idx])


    # p, preds = torch.max(outputs, 0)
    # print(p)
    # print(preds)



if __name__ == "__main__":
        main()

